console.log("Hello World")

// ---------------------------------------------------------------------------------------------

let givenName = "Keima";
console.log("First Name: " + givenName);

let familyName = "Katsuragi";
console.log("Last Name: " + familyName);

let age = 17;
console.log("Age: " + age);

let hobbies = ["playing galge", "conquering flags", "saving goddesses"];
console.log("Hobbies: ");
console.log(hobbies);

let jobLocation = {
	houseNumber: "1912",
	streetName: "Azabudai",
	cityName: "Minato-ku",
	stateName: "Tokyo",
}
console.log("Work Address: ");
console.log(jobLocation);

// ---------------------------------------------------------------------------------------------
// Debugging

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ");
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,

}
console.log("My Full Profile: ");
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

const lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);

